import std.algorithm: min, max;
import std.parallelism: taskPool, defaultPoolThreads;
import std.stdio: writeln;
import core.sync.mutex: Mutex;

void eat(immutable uint i, string name, Mutex[] forks) {
	immutable j = (i+1) % 5;
	writeln(name, " is hungry");
	/* Take forks i and j. The lower one first to prevent deadlock. */
	auto fork1 = forks[min(i,j)];
	auto fork2 = forks[max(i,j)];
	fork1.lock(); scope(exit) fork1.unlock();
	fork2.lock(); scope(exit) fork2.unlock();
	writeln(name, " is eating");
	/* munch munch munch */
	writeln(name, " is full");
}

void think(string name) {
	writeln(name, " is thinking");
	/* hmmmmmmm */
}

void main() {
	string[] philosophers = [ "Kant", "Guatma", "Russel", "Aristotle", "Bart" ];
	Mutex[5] forks;
	foreach(ref fork; forks) fork = new Mutex();

	defaultPoolThreads = 5;
	foreach (uint i, ref philo; taskPool.parallel(philosophers)) {
		foreach(uint j; 0..10000000) {
			eat(i,philo,forks);
			think(philo);
		}
	}
}
