#!/bin/bash
set -e

for p in *.d
do
  if [ -f "$p.exe" ]
  then
    echo "skip $p"
    continue
  fi
  dmd $p -of$p.exe >/dev/null 2>/dev/null || echo "fail $p"
done
