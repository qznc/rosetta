#!/usr/bin/env python3

from urllib.request import urlopen, Request
from urllib.error import HTTPError
from urllib.parse import quote
import json
import os.path

URL = "http://rosettacode.org/mw/api.php?action=query&list=categorymembers&cmtitle=Category:D&cmlimit=500&format=json"
USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.19 (KHTML, like Gecko) Ubuntu/12.04 Chromium/18.0.1025.168 Chrome/18.0.1025.168 Safari/535.19"

def urlget(url):
  r = Request(url, headers={'User-Agent': USER_AGENT})
  return urlopen(r).read().decode("utf8")

def pages():
  raw = urlget(URL)
  js  = json.loads(raw)
  for k in js['query']['categorymembers']:
    yield k['title']

def pageurl(name):
  name = name.replace(" ", "_")
  name = quote(name)
  return "http://www.rosettacode.org/w/index.php?title=%s&action=raw" % name

def get_path(name, count):
  name = name.replace(" ", "_")
  name = name.replace("/", "_")
  name = name.replace("-", "_")
  name = name.replace("'", "")
  return "rosettacode_%s_%02d.d" % (name.lower(), count)

def store_code(path, code):
  with open(path, 'w') as fh:
    fh.write(code)

def process_page(name):
  path = get_path(name, 0)
  if os.path.exists(path):
    return
  url = pageurl(name)
  try:
    raw = urlget(url)
  except HTTPError as e:
    print("skip '%s' due to %s" % (name, e))
    return
  for part in raw.split("\n=={{header|"):
    if not part.startswith("D}}"):
      continue
    code_id = 0
    while True:
      i = part.find("<lang")
      if i == -1:
        if code_id == 0:
          print("Warning: no '<lang' in "+name)
        break
      i = part.index(">", i)
      j = part.find("</lang>", i)
      if j == -1:
        print("Error: no </lang> on "+name)
        print(part)
        continue
      prog = part[i+1:j]
      part = part[j+4:]
      path = get_path(name, code_id)
      store_code(path, prog)
      code_id += 1

if __name__ == "__main__":
  for x in pages():
    process_page(x)
