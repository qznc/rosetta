import std.variant: Variant;
import std.stdio: write, writeln;
import std.conv: text, parse;
import std.uni: isAlpha, isNumber, isWhite;

alias Variant Sexp;

final class Symbol {
	private string name;
	public this(string name) { this.name = name; }
	override public string toString() { return this.name; }
}

bool isIdentChar(char c) pure @safe nothrow {
	if (isAlpha(c)) return true;
	foreach (x; "0123456789!@#-")
		if (c == x) return true;
	return false;
}

Sexp parseSexp(string raw) {
	size_t pos = 0;
	while (isWhite(raw[pos])) pos++;
	Sexp _parse() {
		size_t i = pos+1;
		scope (exit) pos = i;
		if (raw[pos] == '"') {
			while (raw[i] != '"' && i < raw.length) i++;
			i++;
			return Sexp(raw[pos+1..i-1]);
		} else if (isNumber(raw[pos])) {
			while (isNumber(raw[i]) && i < raw.length) i++;
			if (raw[i] == '.') {
				i++;
				while (isNumber(raw[i]) && i < raw.length) i++;
				return Sexp(parse!double(raw[pos..i]));
			}
			return Sexp(parse!ulong(raw[pos..i]));
		} else if (isIdentChar(raw[pos])) {
			while (isIdentChar(raw[i]) && i < raw.length) i++;
			return Sexp(new Symbol(raw[pos..i]));
		} else if (raw[pos] == '(') {
			Sexp[] lst;
			while (raw[i] != ')') {
				while (isWhite(raw[i])) i++;
				pos = i;
				lst ~= _parse();
				i = pos;
				while (isWhite(raw[i])) i++;
			}
			i = pos+1;
			return Sexp(lst);
		}
		return Sexp(null);
	}
	return _parse();
}

void writeSexp(Sexp expr) {
	if (expr.type == typeid(string)) {
		write("\"");
		write(text(expr));
		write("\"");
	} else if (expr.type == typeid(Sexp[])) {
		write("(");
		auto arr = expr.get!(Sexp[]);
		foreach(size_t i, Sexp e; arr) {
			writeSexp(e);
			if (i+1 < arr.length) write(" ");
		}
		write(")");
	} else {
		write(text(expr));
	}
}

void main() {
	auto test = "((data \"quoted data\" 123 4.5)
	 (data (!@# (4.5) \"(more\" \"data)\")))";
	auto ptest = parseSexp(test);
	writeln("parsed: ", ptest);
	write("printed: ");
	writeSexp(ptest); writeln();
}
