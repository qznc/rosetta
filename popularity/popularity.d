import std.stdio, std.algorithm, std.conv, std.array, std.regex,
		 std.typecons, std.net.curl;

void main() {
	// Get a list of just the programming languages.
	auto r1 = regex("\"title\":\"Category:([^\"]+)\"", "g");
	const members = get("www.rosettacode.org/w/api.php?action=query&"~
			"list=categorymembers&cmtitle=Category:Progr"~
			"amming_Languages&cmlimit=500&format=json");
	const languages = members.match(r1).map!q{a[1].dup}().array();

	// Get a pagecount for all categories.
	auto r2 = regex("title=\"Category:([^\"]+)\">[^<]+" ~
			"</a>[^(]+\\((\\d+) members\\)", "g");
	const categories = get("www.rosettacode.org/w/index.php?" ~
			"title=Special:Categories&limit=5000");
	const pairs = categories.match(r2)
		.filter!(m => languages.canFind(m[1]))()
		.map!(m => tuple(m[2].to!uint(), m[1].dup))()
		.array().sort!q{a > b}().release();
	foreach (i, res; pairs)
		writefln("%3d. %3d - %s", i + 1, res.tupleof);
}
