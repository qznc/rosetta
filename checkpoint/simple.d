import std.parallelism: taskPool, defaultPoolThreads, totalCPUs;
import std.stdio: writeln;

void buildMechanism(uint nparts) {
	auto details = new uint[nparts];
	foreach (uint i, ref detail; taskPool.parallel(details)) {
		writeln("Build detail ", i);
		detail = i;
	}
	/* this function could be written more concisely via std.parallelism.reduce,
	   but we want to see the checkpoint explicitly. */
	writeln("Checkpoint reached. Assemble details ...");
	uint sum = 0;
	foreach (ref detail; details) {
		sum += detail;
	}
	writeln("Mechanism with ", nparts, " parts finished: ", sum);
}

void main() {
	defaultPoolThreads = totalCPUs+1; /* totalCPUs-1 is default */
	buildMechanism(42);
	buildMechanism(11);
}
