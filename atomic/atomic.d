import std.stdio: writeln;
import std.conv: text;
import std.random: uniform;
import std.algorithm: min, max;
import std.parallelism: task;
import core.thread: Thread;
import core.sync.mutex: Mutex;
import core.time: dur;

final class Buckets {
	alias TBucketValue = uint;

	private final struct Bucket {
		TBucketValue value;
		Mutex mtx;
		alias value this;
	}

	private Bucket[] buckets;
	private bool running;

	public this(in uint count) {
		this.buckets.length = count;
		this.running = true;
		foreach (ref b; buckets)
			b = Bucket(uniform(0, 100), new Mutex());
	}

	public TBucketValue opIndex(in uint index) const pure nothrow {
		return buckets[index];
	}

	public void transfer(in uint from, in uint to,
			in TBucketValue amount) {
		immutable low  = min(from, to);
		immutable high = max(from, to);

		buckets[low].mtx.lock();
		scope(exit) buckets[low].mtx.unlock();
		buckets[high].mtx.lock();
		scope(exit) buckets[high].mtx.unlock();

		immutable realAmount = min(buckets[from].value, amount);
		buckets[from] -= realAmount;
		buckets[to  ] += realAmount;
	}

	@property size_t length() const pure nothrow {
		return this.buckets.length;
	}

	void toString(in void delegate(const(char)[]) sink) {
		TBucketValue sum = 0;
		foreach (ref b; buckets) {
			b.mtx.lock();
			sum += b;
		}
		scope(exit) foreach (ref b; buckets)
			b.mtx.unlock();
		sink(text(buckets));
		sink(" sum: ");
		sink(text(sum));
	}
}

void randomize(Buckets data) {
	immutable maxi = data.length - 1;

	while (data.running) {
		immutable i = uniform(0, maxi);
		immutable j = uniform(0, maxi);
		immutable amount = uniform(0, 20);
		data.transfer(i, j, amount);
	}
}

void equalize(Buckets data) {
	immutable maxi = data.length - 1;

	while (data.running) {
		immutable i = uniform(0, maxi);
		immutable j = uniform(0, maxi);
		immutable a = data[i];
		immutable b = data[j];
		if (a > b)
			data.transfer(i, j, (a - b) / 2);
		else
			data.transfer(j, i, (b - a) / 2);
	}
}

void display(Buckets data) {
	foreach (i; 0..20) {
		writeln(data);
		Thread.sleep(dur!"msecs"(200));
	}
	data.running = false;
}

void main() {
	auto data = new Buckets(20);
	task!randomize(data).executeInNewThread();
	task!equalize(data).executeInNewThread();
	task!display(data).executeInNewThread();
}
